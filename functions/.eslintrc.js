module.exports = {
  root: true,
  env: {
    es6: true,
    node: true,
  },
  extends: [
    "eslint:recommended",
    "google",
  ],
  parserOptions: {
    "ecmaVersion": 2019,
  },
  rules: {
    "prefer-const": "off",
    "max-len": "off",
    "valid-jsdoc": "off",
    "require-jsdoc": "off",
    "quotes": ["error", "double"],
  },
};
