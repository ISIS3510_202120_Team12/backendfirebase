const functions = require("firebase-functions");
const express = require("express");
// const cors = require("cors");

const admin = require("firebase-admin");
admin.initializeApp();

const app = express();
const appInfoMedica = express();

// User information
app.get("/", async (req, res) => {
  const snapshot = await admin.firestore().collection("users").get();

  let users = [];
  snapshot.forEach((doc) => {
    let id = doc.id;
    console.log(doc.data);
    let data = doc.data();

    users.push({id, ...data});
  });

  res.status(200).send(JSON.stringify(users));
});
app.get("/:id", async (req, res) => {
  const snapshot = await admin.firestore().collection("users").doc(req.params.id).get();
  const userid = snapshot.id;
  const userData = snapshot.data();
  res.status(200).send(JSON.stringify({id: userid, ...userData}));
});
app.put("/:id", async (req, res)=>{
  const body = req.body;
  await admin.firestore().collection("users").doc(req.params.id).set({body});
  res.status(201).send();
});
app.delete("/:id", async (req, res) =>{
  await admin.firestore().collection("users").doc(req.params.id).delete();
  res.status(200).send();
});
app.post("/", async (req, res) => {
  const user = req.body;
  await admin.firestore().collection("users").add(user);
  res.status(201).send();
});

exports.user = functions.https.onRequest(app);


// información médica

appInfoMedica.get("/", async (req, res) => {
  const snapshot = await admin.firestore().collection("info_medica").get();
  let infoMedica = [];
  snapshot.forEach((doc) => {
    let id = doc.id;
    let data = doc.data();
    infoMedica.push({id, ...data});
  });
  res.status(200).send(JSON.stringify(infoMedica));
});
appInfoMedica.get("/:id", async (req, res) => {
  const snapshot = await admin.firestore().collection("info_medica").doc(req.params.id).get();
  const infoMedicaId = snapshot.id;
  const infoMedicaData = snapshot.data();
  res.status(200).send(JSON.stringify({id: infoMedicaId, ...infoMedicaData}));
});
appInfoMedica.put("/:id", async (req, res)=>{
  const body = req.body;
  await admin.firestore().collection("info_medica").doc(req.params.id).set({body});
  res.status(201).send();
});

appInfoMedica.delete("/:id", async (req, res) =>{
  await admin.firestore().collection("info_medica").doc(req.params.id).delete();
  res.status(200).send();
});
appInfoMedica.post("/", async (req, res) => {
  const infoMedica = req.body;
  await admin.firestore().collection("info_medica").add(infoMedica);
  res.status(201).send();
});
exports.medicalInfo = functions.https.onRequest(appInfoMedica);

// AppPreguntas
const appPreguntas = express();
appPreguntas.get("/", async (req, res) => {
  const snapshot = await admin.firestore().collection("preguntas").get();
  let preguntas = [];
  snapshot.forEach((doc) => {
    let id = doc.id;
    let data = doc.data();
    preguntas.push({id, ...data});
  });
  res.status(200).send(JSON.stringify(preguntas));
});
appPreguntas.get("/:id", async (req, res) => {
  const snapshot = await admin.firestore().collection("preguntas").doc(req.params.id).get();
  const preguntasId = snapshot.id;
  const preguntasData = snapshot.data();
  res.status(200).send(JSON.stringify({id: preguntasId, ...preguntasData}));
});
appPreguntas.put("/:id", async (req, res) => {
  const body = req.body;
  await admin.firestore().collection("preguntas").doc(req.params.id).set({body});
  res.status(201).send();
});
appPreguntas.delete("/:id", async (req, res) => {
  await admin.firestore().collection("preguntas").doc(req.params.id).delete();
  res.status(200).send();
});
appPreguntas.post("/", async (req, res) => {
  const preguntas = req.body;
  await admin.firestore().collection("preguntas").add(preguntas);
  res.status(201).send();
});
exports.preguntas = functions.https.onRequest(appPreguntas);

// AppPreferencias
const appPreferencias = express();
appPreferencias.get("/", async (req, res) => {
  const snapshot = await admin.firestore().collection("preferencias").get();
  let preferencias = [];
  snapshot.forEach((doc) => {
    let id = doc.id;
    let data = doc.data();
    preferencias.push({id, ...data});
  });
  res.status(200).send(JSON.stringify(preferencias));
});
appPreferencias.get("/:id", async (req, res) => {
  const snapshot = await admin.firestore().collection("preferencias").doc(req.params.id).get();
  const preferenciasId = snapshot.id;
  const preferenciasData = snapshot.data();
  res.status(200).send(JSON.stringify({id: preferenciasId, ...preferenciasData}));
});
appPreferencias.put("/:id", async (req, res) => {
  const body = req.body;
  await admin.firestore().collection("preferencias").doc(req.params.id).set({body});
  res.status(201).send();
});
appPreferencias.delete("/:id", async (req, res) => {
  await admin.firestore().collection("preferencias").doc(req.params.id).delete();
  res.status(200).send();
});
appPreferencias.post("/", async (req, res) => {
  const preferencias = req.body;
  await admin.firestore().collection("preferencias").add(preferencias);
  res.status(201).send();
});
exports.preferencias = functions.https.onRequest(appPreferencias);

// AppTips
const appTips = express();
appTips.get("/", async (req, res) => {
  const snapshot = await admin.firestore().collection("tips").get();
  let tips = [];
  snapshot.forEach((doc) => {
    let id = doc.id;
    let data = doc.data();
    tips.push({id, ...data});
  });
  res.status(200).send(JSON.stringify(tips));
});
appTips.get("/:id", async (req, res) => {
  const snapshot = await admin.firestore().collection("tips").doc(req.params.id).get();
  const tipsId = snapshot.id;
  const tipsData = snapshot.data();
  res.status(200).send(JSON.stringify({id: tipsId, ...tipsData}));
});
appTips.put("/:id", async (req, res) => {
  const body = req.body;
  await admin.firestore().collection("tips").doc(req.params.id).update({
    ...body});
  res.status(201).send();
});
appTips.delete("/:id", async (req, res) => {
  await admin.firestore().collection("tips").doc(req.params.id).delete();
  res.status(200).send();
});
appTips.post("/", async (req, res) => {
  const tips = req.body;
  await admin.firestore().collection("tips").add(tips);
  res.status(201).send();
});
exports.tips = functions.https.onRequest(appTips);

// ContactoEmergencia
const contactoEmergencia = express();

contactoEmergencia.get("/:id", async (req, res) => {
  const snapshot = await admin.firestore().collection("contacto").doc(req.params.id).get();
  const tipsId = snapshot.id;
  const tipsData = snapshot.data();
  res.status(200).send(JSON.stringify({id: tipsId, ...tipsData}));
});
contactoEmergencia.put("/:id", async (req, res) => {
  const body = req.body;
  await admin.firestore().collection("contacto").doc(req.params.id).update({
    ...body});
  res.status(201).send();
});

exports.contacto = functions.https.onRequest(contactoEmergencia);

// Calculos
const calculos = express();
calculos.get("/", async (req, res) => {
  const snapshot = await admin.firestore().collection("calculos").get();
  let tips = [];
  snapshot.forEach((doc) => {
    let id = doc.id;
    let data = doc.data();
    tips.push({id, ...data});
  });
  res.status(200).send(JSON.stringify(tips));
});
calculos.get("/:id", async (req, res) => {
  const snapshot = await admin.firestore().collection("calculos").doc(req.params.id).get();
  const tipsId = snapshot.id;
  const tipsData = snapshot.data();
  res.status(200).send(JSON.stringify({id: tipsId, ...tipsData}));
});
calculos.put("/:id", async (req, res) => {
  const body = req.body;
  await admin.firestore().collection("tips").doc(req.params.id).update({
    ...body});
  res.status(201).send();
});

exports.calculos = functions.https.onRequest(calculos);

// HTTP Request Calculator
exports.RiskCalculator = functions.https.onRequest((request, response) =>{
  let value = 0;
  const body = request.body;
  const altura = Number(body.altura);
  const colesterolTotal = Number(body.colesterolTotal);
  const consumoAlcohol = JSON.parse(body.consumoAlcohol);
  const diabetico = JSON.parse(body.diabetico);
  const fumador = JSON.parse(body.fumador);
  const peso= Number(body.peso);
  const presionSistolica = Number(body.presionSistolica);
  const genero = body.genero;
  const spo2= body.spo2;
  const fechaNacimiento = body.fechaNacimiento;
  let IMC=peso/Math.pow(altura, 2);
  let returnValue=1;
  if (IMC >29.9) {
    value += 2;
  } else if (IMC>24.9) {
    value+=1;
  }
  if (spo2<90) {
    value+=1;
  }
  if (consumoAlcohol) {
    value+=1;
  }
  if (diabetico) {
    value+=1;
  }
  if (fumador) {
    value+=1;
  }
  if (genero=="M") {
    value+=1;
  }
  if (presionSistolica>140) {
    value+=2;
  } else if (presionSistolica>120) {
    value+=1;
  }
  if (colesterolTotal>239) {
    value+=2;
  } else if (colesterolTotal>200) {
    value+=1;
  }

  const age= getAge(fechaNacimiento);
  if (age>65) {
    value+=1;
  }
  if (value>8) {
    returnValue=3;
  } else if (value>5) {
    returnValue=2;
  }
  response.send(returnValue.toString());
});


function getAge(dateString) {
  let today = new Date();
  let birthDate = new Date(dateString);
  let age = today.getFullYear() - birthDate.getFullYear();
  let m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  return age;
}
